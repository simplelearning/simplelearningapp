package com.example.guiberto.simplelearningapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by franklin on 14/04/16.
 */
public class CustomAdapter extends BaseAdapter {

    private ArrayList<NewsListView_Cell> listData;
    private LayoutInflater layoutInflater;

    public CustomAdapter(Context context, ArrayList<NewsListView_Cell> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = layoutInflater.inflate(R.layout.activity_newslistview,null);
            holder = new ViewHolder();
            holder.txt_Description = (TextView)view.findViewById(R.id.row_decription);
            holder.txt_Date = (TextView)view.findViewById(R.id.row_date);
            holder.txt_title = (TextView)view.findViewById(R.id.row_title);
            view.setTag(holder);
        }else {
            holder = (ViewHolder)view.getTag();
        }
        holder.txt_Description.setText(listData.get(i).str_description);
        holder.txt_title.setText(listData.get(i).str_title);
        holder.txt_Date.setText(listData.get(i).str_date);
        return view;
    }
    static class ViewHolder {
        TextView txt_title;
        TextView txt_Description;
        TextView txt_Date;
    }
}

package com.example.guiberto.simplelearningapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UnitActivity extends AppCompatActivity {
    private DatabaseReference mFirebaseDatabaseReference;
    private String completionrule;
    List<String> m_typeList;
    int m_typeIndex = 0;
    private String unitKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unitscreen);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        m_typeList = new ArrayList<String>();

        //Intent intent = getIntent();
        //final int completestatus = intent.getExtras().getInt("completestatus");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int value = bundle.getInt("Index");
            if (value == 0)
                setTitle("Math");
            else if (value == 1)
                setTitle("Business");
            else if (value == 2)
                setTitle("English");
        }
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
//        DatabaseReference completesdata = mFirebaseDatabaseReference.child("v1/users/"+Global.currentUser.userId );
//
//        completesdata.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                //ListView view = (ListView)findViewById(R.id.subjectlist);
//                int count = (int)dataSnapshot.getChildrenCount();
//                String successquiz = dataSnapshot.getKey();
//                if(count > 1)
//                {
//                    DatabaseReference datacomplete = mFirebaseDatabaseReference.child("v1/users/"+Global.currentUser.userId + successquiz);
//                    datacomplete.addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            UserQuizView_Cell unitdata = dataSnapshot.getValue(UserQuizView_Cell.class);
//                            statusquiz = unitdata.getCompleteStatus();
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//                            Log.w("Test", "Error loading data:" + databaseError.toString(), databaseError.toException());
//                        }
//                    });
//                }
////                UserQuizStatus quiz = dataSnapshot.getValue(UserQuizStatus.class);
////                //UserQuizStatus unitData = new UserQuizStatus();
////                //unitData.CompleteStatusA = quiz.CompleteStatusA;
////                String username = quiz.getUsername();
////                //Log.d("log", quiz.getCompleteStatusA());
////                if(count > 1) {
////                    statusquiz1 = quiz.getCompleteStatusA();
////                    //statusquiz2 = quiz.getCompleteStatusB();
//             //   }
//            }
//            @Override
//            public void onCancelled(DatabaseError error) {
//                Log.w("Test", "Error loading data:" + error.toString(), error.toException());
//            }
//        });

        //mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference v1 = mFirebaseDatabaseReference.child("v1/units");

        v1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    final ListView view = (ListView) findViewById(R.id.unitlist);

                    final ArrayList<UnitListView_Cell> results = new ArrayList<UnitListView_Cell>();

                    Iterable<DataSnapshot> units = dataSnapshot.getChildren();

                    for (DataSnapshot unit : units) {
                        unitKey = unit.getKey();
                        m_typeList.add(unitKey);

                        UnitListView_Cell chat = unit.getValue(UnitListView_Cell.class);

                        final UnitListView_Cell unitData = new UnitListView_Cell();
                        unitData.title = chat.getTitle();
                        unitData.completion_rule = chat.getCompletion_rule();
                        results.add(unitData);

                        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
                        DatabaseReference datacomplete = mFirebaseDatabaseReference.child("v1/users/" + Global.currentUser.userId + "/" + Global.currentUser.coursekey + "/" + unitKey);

                        datacomplete.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    UserQuizView_Cell unitdata1 = dataSnapshot.getValue(UserQuizView_Cell.class);
                                    //UserQuizView_Cell unitdata1 = dataSnapshot.getValue(UserQuizView_Cell.class);
                                    if (unitdata1 != null) {
                                        unitData.setCompleteStatus(unitdata1.isCompleteStatus());

                                        setUnitListView(view, results);
                                    }
                                }
                               // setUnitListView(view, results);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.w("Test", "Error loading data:" + databaseError.toString(), databaseError.toException());
                            }
                        });

                        Log.d("log", chat.getTitle());
                    }

                    setUnitListView(view, results);
                } catch (Exception e) {
                    Log.d("log", "onDataChange Error happen:" + e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("Test", "Error loading data:" + error.toString(), error.toException());
            }
        });


//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("v1/users/us900");
//
//        myRef.setValue("Hello, World!");

    }


    public void setUnitListView(View view, final ArrayList<UnitListView_Cell> results){
        final ListView user_unitlists = (ListView) view;
        user_unitlists.setAdapter(new UnitAdapter(getBaseContext(), results));
        user_unitlists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(UnitActivity.this, ComponentsActivity.class);
                intent.putExtra("Index",position);
                //intent.putExtra("QuizKey", m_typeList.get(position));
                Global.currentUser.quizKey = m_typeList.get(position);
                Global.currentUser.isAllUnit = results.get(position).completion_rule.equals("all");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in.
        // TODO: Add code to check if user is signed in.
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void onBack()
    {
        Intent nextActivity = new Intent(UnitActivity.this, HomeScreenActivity.class);
        startActivity(nextActivity);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


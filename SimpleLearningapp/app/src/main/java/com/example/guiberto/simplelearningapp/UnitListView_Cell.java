package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 5/12/16.
 */
public class UnitListView_Cell {

    String title;
    String completion_rule;
    boolean completeStatus;

    public UnitListView_Cell(){
        this.completeStatus = false;
    }
    public UnitListView_Cell(String title, String completion_rule) {
        this.title = title;
        this.completion_rule = completion_rule;
        this.completeStatus = false;
    }

    public boolean getCompleteStatus() {
        return completeStatus;
    }

    public void setCompleteStatus(boolean completeStatus) {
        this.completeStatus = completeStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompletion_rule() {
        return completion_rule;
    }

    public void setCompletion_rule(String completion_rule) {
        this.completion_rule = completion_rule;
    }
}

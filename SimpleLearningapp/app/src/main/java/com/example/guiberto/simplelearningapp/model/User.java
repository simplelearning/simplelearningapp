package com.example.guiberto.simplelearningapp.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by mymac on 5/24/16.
 */
@IgnoreExtraProperties
public class User {
    public String username;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username) {
        this.username = username;
    }
}

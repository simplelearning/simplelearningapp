package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 4/28/16.
 */
public class UserInfoClass {
    public String strname;
    public String stremail;
    public String userId;
    public String quizKey;
    public String coursekey;
    public int statusflag;
    public Boolean isAllUnit;
    public String videoid;
    public UserInfoClass(){
        strname = "";
        stremail = "";
        userId = "";
        quizKey = "";
        coursekey="";
        statusflag = 0;
        isAllUnit = false;
    }

}

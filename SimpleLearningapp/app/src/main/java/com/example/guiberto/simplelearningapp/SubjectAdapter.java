package com.example.guiberto.simplelearningapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by mymac on 4/29/16.
 */
public class SubjectAdapter extends BaseAdapter {
    private ArrayList<SubListView_Cell> listData;
    private LayoutInflater layoutInflater;

    public SubjectAdapter(Context context, ArrayList<SubListView_Cell> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = layoutInflater.inflate(R.layout.activity_sublistview,null);
            holder = new ViewHolder();
            holder.txt_subTitle = (TextView)view.findViewById(R.id.sub_title);
            view.setTag(holder);
        }else {
            holder = (ViewHolder)view.getTag();
        }
        holder.txt_subTitle.setText(listData.get(i).name);
        return view;
    }
    static class ViewHolder {
        TextView txt_subTitle;
        //TextView txt_subDescription;
    }
}

package com.example.guiberto.simplelearningapp;

import android.app.Application;

import com.facebook.FacebookSdk;


/**
 * Created by dragon on 4/18/16.
 */
public class LMSApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
    }
}

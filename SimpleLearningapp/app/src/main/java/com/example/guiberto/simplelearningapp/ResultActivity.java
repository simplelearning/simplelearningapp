package com.example.guiberto.simplelearningapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ResultActivity extends AppCompatActivity {
    public boolean flag;
    private DatabaseReference myRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
       // boolean isAllUnit = //intent.getExtras().getBoolean("isAllUnit", false);

        int rightvalue = intent.getExtras().getInt("result1");
        int wrongvalue = intent.getExtras().getInt("result2");
        int queCount = intent.getExtras().getInt("quecount");
        TextView resulttitle=  (TextView)findViewById(R.id.resulttitle);
        TextView right=  (TextView)findViewById(R.id.rightcount);
        TextView wrong=  (TextView)findViewById(R.id.wrongcount);
        myRef = FirebaseDatabase.getInstance().getReference();

        flag = Global.currentUser.isAllUnit ? (rightvalue == queCount) : (rightvalue > 0);
        myRef.child("v1/users/"+Global.currentUser.userId).child(Global.currentUser.coursekey).child(Global.currentUser.quizKey).child("completeStatus").setValue(flag);

        Button button = (Button)findViewById(R.id.nextscreen);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextintent = new Intent(ResultActivity.this, UnitActivity.class);
                nextintent.putExtra("completestatus",flag);
                startActivity(nextintent);
            }
        });
        right.setText(String.format("%d", rightvalue));
        wrong.setText(String.format("%d", wrongvalue));


        //wrong.setText(activity.wrong);
    }
}

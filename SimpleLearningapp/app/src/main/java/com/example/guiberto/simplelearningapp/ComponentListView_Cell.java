package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 5/26/16.
 */
public class ComponentListView_Cell {
    String type;

    public ComponentListView_Cell(){
    }
    public ComponentListView_Cell(String type) {
        this.type = type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getType() {
        return type;
    }
}

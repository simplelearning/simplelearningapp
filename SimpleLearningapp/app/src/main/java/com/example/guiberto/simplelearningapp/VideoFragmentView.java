package com.example.guiberto.simplelearningapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.github.booknara.youtube.*;
import com.github.booknara.youtube.BuildConfig;
import com.github.booknara.youtube.PlaylistId;
import com.github.booknara.youtube.VideoId;
import com.github.booknara.youtube.YouTubeId;
import com.github.booknara.youtube.YouTubeUtility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;

public class VideoFragmentView extends Fragment {
    public int index;

    public static final String SCHEME_YOUTUBE_VIDEO = "ytv";
    public static final String SCHEME_YOUTUBE_PLAYLIST = "ytpl";

    static final String YOUTUBE_VIDEO_INFORMATION_URL = "http://www.youtube.com/get_video_info?&video_id=";
    static final String YOUTUBE_PLAYLIST_ATOM_FEED_URL = "http://gdata.youtube.com/feeds/api/playlists/";

    protected ProgressBar mProgressBar;
    protected TextView mProgressMessage;
    protected VideoView mVideoView;

    public final static String MSG_INIT = "com.keyes.video.msg.init";
    protected String      mMsgInit       = "Initializing";

    public final static String MSG_DETECT = "com.keyes.video.msg.detect";
    protected String      mMsgDetect     = "Detecting Bandwidth";

    public final static String MSG_PLAYLIST = "com.keyes.video.msg.playlist";
    protected String      mMsgPlaylist   = "Determining Latest Video in YouTube Playlist";

    public final static String MSG_TOKEN = "com.keyes.video.msg.token";
    protected String      mMsgToken      = "Retrieving YouTube Video Token";

    public final static String MSG_LO_BAND = "com.keyes.video.msg.loband";
    protected String      mMsgLowBand    = "Buffering Low-bandwidth Video";

    public final static String MSG_HI_BAND = "com.keyes.video.msg.hiband";
    protected String      mMsgHiBand     = "Buffering High-bandwidth Video";

    public final static String MSG_ERROR_TITLE = "com.keyes.video.msg.error.title";
    protected String      mMsgErrorTitle = "Communications Error";

    public final static String MSG_ERROR_MSG = "com.keyes.video.msg.error.msg";
    protected String      mMsgError      = "An error occurred during the retrieval of the video.  This could be due to network issues or YouTube protocols.  Please try again later.";

    /** Background task on which all of the interaction with YouTube is done */
    protected QueryYouTubeTask mQueryYouTubeTask;

    protected String mVideoId = null;


    private ImageView imageView;
    private int currentPage = 0;
    private Button next, previous;

    public void setIndex(int index) {
        this.index = index;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;

        if(index == 0)
        {
            view = inflater.inflate(R.layout.activity_video_fragment_view,
                    container, false);

            Uri lVideoIdUri = Uri.parse("ytv://" + Global.currentUser.videoid);

            mVideoView = (VideoView) view.findViewById(R.id.showvideo);

            String lVideoSchemeStr = lVideoIdUri.getScheme();
            String lVideoIdStr     = lVideoIdUri.getEncodedSchemeSpecificPart();
            if(lVideoIdStr == null) {
                Log.i(this.getClass().getSimpleName(), "No video ID was specified in the intent.  Closing video activity.");
            }
            if(lVideoIdStr.startsWith("//")){
                if(lVideoIdStr.length() > 2){
                    lVideoIdStr = lVideoIdStr.substring(2);
                } else {
                    Log.i(this.getClass().getSimpleName(), "No video ID was specified in the intent.  Closing video activity.");
                }
            }

            YouTubeId lYouTubeId = null;
            if(lVideoSchemeStr != null && lVideoSchemeStr.equalsIgnoreCase(SCHEME_YOUTUBE_PLAYLIST)){
                lYouTubeId = new PlaylistId(lVideoIdStr);
            }

            else if(lVideoSchemeStr != null && lVideoSchemeStr.equalsIgnoreCase(SCHEME_YOUTUBE_VIDEO)){
                lYouTubeId = new VideoId(lVideoIdStr);
            }

            if(lYouTubeId == null){
                Log.i(this.getClass().getSimpleName(), "Unable to extract video ID from the intent.  Closing video activity.");
                //finish();
            }

            mQueryYouTubeTask = (QueryYouTubeTask) new QueryYouTubeTask().execute(lYouTubeId);

            WebView webView = (WebView) view.findViewById(R.id.pdfView1);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            String linkTo = "www.effortlessenglishclub.com/podcast/Powerful%20English%20Speaking.pdf";
            webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + linkTo);
        }

        else if(index == 1)
        {
            view = inflater.inflate(R.layout.activity_audio_fragment_view, container, false);

            WebView mSoundCloudPlayer =(WebView) view.findViewById(R.id.audioView);

            String trackID = "264097257";

            String embedURL = "<iframe width=\"100%\" height=\"450\" scrolling=\"no\" frameborder=\"no\" " +
                    "src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/" + trackID +
                    "&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>";

            String html = "<!DOCTYPE html><html> <head> <meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"target-densitydpi=high-dpi\" /> " +
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <link rel=\"stylesheet\" media=\"screen and (-webkit-device-pixel-ratio:1.5)\" href=\"hdpi.css\" /></head> " +
                    "<body style=\"background:black;margin:0 0 0 0; padding:0 0 0 0;\">" + embedURL +
                    "<script src=\"https://w.soundcloud.com/player/api.js\" type=\"text/javascript\"></script> </body> </html> ";

            mSoundCloudPlayer.setVisibility(View.VISIBLE);
            mSoundCloudPlayer.getSettings().setJavaScriptEnabled(true);
            mSoundCloudPlayer.getSettings().setLoadWithOverviewMode(true);
            mSoundCloudPlayer.getSettings().setUseWideViewPort(true);
            mSoundCloudPlayer.loadDataWithBaseURL("",html,"text/html", "UTF-8", "");


            WebView webView = (WebView) view.findViewById(R.id.pdfView2);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            String linkTo = "www.effortlessenglishclub.com/podcast/Powerful%20English%20Speaking.pdf";
            webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + linkTo);
        }

        else if(index == 2)
        {
            view = inflater.inflate(R.layout.activity_slides_fragment_view,
                    container, false);
            WebView webView = (WebView) view.findViewById(R.id.pdfView);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            String linkTo = "www.effortlessenglishclub.com/podcast/Powerful%20English%20Speaking.pdf";
            webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + linkTo);



//            imageView = (ImageView) view.findViewById(R.id.imageView);
//            next = (Button) view.findViewById(R.id.next);
//            next.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    currentPage++;
//                    render();
//                }
//            });
//            previous = (Button) view.findViewById(R.id.previous);
//            previous.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    currentPage--;
//                    render();
//                }
//            });
//            render();
        }

        return view;
    }

    /*private void render() {
        try {

            int REQ_WIDTH = 1;
            int REQ_HEIGHT = 1;
            REQ_WIDTH = imageView.getWidth();
            REQ_HEIGHT = imageView.getHeight();

            Bitmap bitmap = Bitmap.createBitmap(REQ_WIDTH, REQ_HEIGHT, Bitmap.Config.ARGB_4444);
            File file = new File("/sdcard/Downloads/test.pdf");
            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));

            if (currentPage < 0) {
                currentPage = 0;
            } else if (currentPage > renderer.getPageCount()) {
                currentPage = renderer.getPageCount() - 1;
            }

            Matrix m = imageView.getImageMatrix();
            Rect rect = new Rect(0, 0, REQ_WIDTH, REQ_HEIGHT);
            renderer.openPage(currentPage).render(bitmap, rect, m, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            imageView.setImageMatrix(m);
            imageView.setImageBitmap(bitmap);
            imageView.invalidate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
//    private void setupView() {
//        LinearLayout lLinLayout = new LinearLayout(this);
//        lLinLayout.setId(1);
//        lLinLayout.setOrientation(LinearLayout.VERTICAL);
//        lLinLayout.setGravity(Gravity.CENTER);
//        lLinLayout.setBackgroundColor(Color.BLACK);
//
//        LinearLayout.LayoutParams lLinLayoutParms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        lLinLayout.setLayoutParams(lLinLayoutParms);
//
//        this.setContentView(lLinLayout);
//
//
//        RelativeLayout lRelLayout = new RelativeLayout(this);
//        lRelLayout.setId(2);
//        lRelLayout.setGravity(Gravity.CENTER);
//        lRelLayout.setBackgroundColor(Color.BLACK);
//        android.widget.RelativeLayout.LayoutParams lRelLayoutParms = new android.widget.RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        lRelLayout.setLayoutParams(lRelLayoutParms);
//        lLinLayout.addView(lRelLayout);
//
////        mVideoView = new VideoView(this);
////        mVideoView.setId(3);
////        android.widget.RelativeLayout.LayoutParams lVidViewLayoutParams = new android.widget.RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
////        lVidViewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
////        mVideoView.setLayoutParams(lVidViewLayoutParams);
////        lRelLayout.addView(mVideoView);
//
//
//        mProgressBar = new ProgressBar(this);
//        mProgressBar.setIndeterminate(true);
//        mProgressBar.setVisibility(View.VISIBLE);
//        mProgressBar.setEnabled(true);
//        mProgressBar.setId(4);
//        android.widget.RelativeLayout.LayoutParams lProgressBarLayoutParms = new android.widget.RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        lProgressBarLayoutParms.addRule(RelativeLayout.CENTER_IN_PARENT);
//        mProgressBar.setLayoutParams(lProgressBarLayoutParms);
//        lRelLayout.addView(mProgressBar);
//
//        mProgressMessage = new TextView(this);
//        mProgressMessage.setId(5);
//        android.widget.RelativeLayout.LayoutParams lProgressMsgLayoutParms = new android.widget.RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        lProgressMsgLayoutParms.addRule(RelativeLayout.CENTER_HORIZONTAL);
//        lProgressMsgLayoutParms.addRule(RelativeLayout.BELOW, 4);
//        mProgressMessage.setLayoutParams(lProgressMsgLayoutParms);
//        mProgressMessage.setTextColor(Color.LTGRAY);
//        mProgressMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
//        mProgressMessage.setText("...");
//        lRelLayout.addView(mProgressMessage);
//    }

    public void updateProgress(String pProgressMsg){
        try {
            mProgressMessage.setText(pProgressMsg);
        } catch(Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error updating video status!", e);
        }
    }

    private class ProgressUpdateInfo {

        public String mMsg;

        public ProgressUpdateInfo(String pMsg){
            mMsg = pMsg;
        }
    }

    private class QueryYouTubeTask extends AsyncTask<com.github.booknara.youtube.YouTubeId, ProgressUpdateInfo, Uri> {

        private boolean mShowedError = false;

        @Override
        protected Uri doInBackground(YouTubeId... pParams) {
            String lUriStr = null;
            String lYouTubeFmtQuality = "17";   // 3gpp medium quality, which should be fast enough to view over EDGE connection
            String lYouTubeVideoId = null;

            if(isCancelled())
                return null;

            try {

                publishProgress(new ProgressUpdateInfo(mMsgDetect));

                WifiManager lWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
                TelephonyManager lTelephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

                ////////////////////////////
                // if we have a fast connection (wifi or 3g), then we'll get a high quality YouTube video
                if( (lWifiManager.isWifiEnabled() && lWifiManager.getConnectionInfo() != null && lWifiManager.getConnectionInfo().getIpAddress() != 0) ||
                        ( (lTelephonyManager.getNetworkType() == TelephonyManager.NETWORK_TYPE_UMTS ||

					   /* icky... using literals to make backwards compatible with 1.5 and 1.6 */
                                lTelephonyManager.getNetworkType() == 9 /*HSUPA*/  ||
                                lTelephonyManager.getNetworkType() == 10 /*HSPA*/  ||
                                lTelephonyManager.getNetworkType() == 8 /*HSDPA*/  ||
                                lTelephonyManager.getNetworkType() == 5 /*EVDO_0*/  ||
                                lTelephonyManager.getNetworkType() == 6 /*EVDO A*/)

                                && lTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED)
                        ){
                    lYouTubeFmtQuality = "18";
                }


                ///////////////////////////////////
                // if the intent is to show a playlist, get the latest video id from the playlist, otherwise the video
                // id was explicitly declared.
                if(pParams[0] instanceof PlaylistId){
                    publishProgress(new ProgressUpdateInfo(mMsgPlaylist));
                    lYouTubeVideoId = YouTubeUtility.queryLatestPlaylistVideo((PlaylistId) pParams[0]);
                }

                else if(pParams[0] instanceof VideoId){
                    lYouTubeVideoId = pParams[0].getId();
                }

                mVideoId = lYouTubeVideoId;

                publishProgress(new ProgressUpdateInfo(mMsgToken));

                if(isCancelled())
                    return null;

                ////////////////////////////////////
                // calculate the actual URL of the video, encoded with proper YouTube token
                lUriStr = YouTubeUtility.calculateYouTubeUrl(lYouTubeFmtQuality, true, lYouTubeVideoId);

                if(isCancelled())
                    return null;

                if(lYouTubeFmtQuality.equals("17")){
                    publishProgress(new ProgressUpdateInfo(mMsgLowBand));
                } else {
                    publishProgress(new ProgressUpdateInfo(mMsgHiBand));
                }

            } catch(Exception e) {
                Log.e(this.getClass().getSimpleName(), "Error occurred while retrieving information from YouTube.", e);
            }

            if(lUriStr != null){
                return Uri.parse(lUriStr);
            } else {
                return null;
            }
        }



        @Override
        protected void onPostExecute(Uri pResult) {
            super.onPostExecute(pResult);

            try {
                if(isCancelled())
                    return;

                if(pResult == null){
                    throw new RuntimeException("Invalid NULL Url.");
                }

                Log.i(this.getClass().getSimpleName(), pResult.toString());

                mVideoView.setVideoURI(pResult);

                if(isCancelled())
                    return;

                // TODO:  add listeners for finish of video
                mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){

                    @Override
                    public void onCompletion(MediaPlayer pMp) {
                        if(isCancelled())
                            return;
                        getActivity().finish();
                    }

                });

                if(isCancelled())
                    return;

                final MediaController lMediaController = new MediaController(getActivity());
                mVideoView.setMediaController(lMediaController);
                lMediaController.show(0);
                //mVideoView.setKeepScreenOn(true);
                mVideoView.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer pMp) {
                        if(isCancelled())
                            return;
//                        getActivity().mProgressBar.setVisibility(View.GONE);
//                        getActivity().mProgressMessage.setVisibility(View.GONE);
                    }

                });

                if(isCancelled())
                    return;

                mVideoView.requestFocus();
                mVideoView.start();
            } catch(Exception e){
                Log.e(this.getClass().getSimpleName(), "Error playing video!", e);

                if(!mShowedError){
                    showErrorAlert();
                }
            }
        }

        private void showErrorAlert() {

            try {
                AlertDialog.Builder lBuilder = new AlertDialog.Builder(getActivity());
                lBuilder.setTitle(mMsgErrorTitle);
                lBuilder.setCancelable(false);
                lBuilder.setMessage(mMsgError);

                lBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface pDialog, int pWhich) {
                        getActivity().finish();
                    }

                });

                AlertDialog lDialog = lBuilder.create();
                lDialog.show();
            } catch(Exception e){
                Log.e(this.getClass().getSimpleName(), "Problem showing error dialog.", e);
            }
        }

        @Override
        protected void onProgressUpdate(ProgressUpdateInfo... pValues) {
            super.onProgressUpdate(pValues);

            VideoFragmentView.this.updateProgress(pValues[0].mMsg);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

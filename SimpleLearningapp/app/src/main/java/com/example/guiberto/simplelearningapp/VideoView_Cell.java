package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 5/16/16.
 */
public class VideoView_Cell
{
    private String video_id;

    public VideoView_Cell(){

    }

    public VideoView_Cell(String videoid)
    {
        this.video_id = videoid;
    }

    public String getVideo_id(){
        return video_id;
    }

    public void setVideo_id(String videoid){
        this.video_id = videoid;
    }

}

package com.example.guiberto.simplelearningapp;

import android.app.ListFragment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class FragmentText extends Fragment {

    public int index;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general,
               container, false);

        if(index == 0){
            setNewsListView(view, initializeList());
        }
        else{
            setNewsListView(view, initializeList2());
        }
        return view;
    }
    public void setNewsListView(View view, ArrayList<NewsListView_Cell>results){
        final ListView user_generallists = (ListView)(view.findViewById(R.id.newslist));
        user_generallists.setAdapter(new CustomAdapter(this.getContext(), results));
        user_generallists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Object o = user_generallists.getItemAtPosition(position);
                NewsListView_Cell newsData = (NewsListView_Cell) o;
//                Toast.makeText(, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
            }
        });
    }
    public ArrayList<NewsListView_Cell> initializeList(){
        ArrayList<NewsListView_Cell> results = new ArrayList<NewsListView_Cell>();
        NewsListView_Cell newsData = new NewsListView_Cell();
        newsData.str_title = "1";
        newsData.str_description = "You liked";
        newsData.str_date = "5h";

        results.add(newsData);

        newsData = new NewsListView_Cell();
        newsData.str_title = "2";
        newsData.str_description = "You disliked";
        newsData.str_date = "5h";

        results.add(newsData);
        return results;
    }

    public ArrayList<NewsListView_Cell> initializeList2(){
        ArrayList<NewsListView_Cell> results = new ArrayList<NewsListView_Cell>();
        NewsListView_Cell newsData = new NewsListView_Cell();
        newsData.str_title = "3";
        newsData.str_description = "You liked";
        newsData.str_date = "5h";

        results.add(newsData);

        newsData = new NewsListView_Cell();
        newsData.str_title = "4";
        newsData.str_description = "You disliked";
        newsData.str_date = "5h";

        results.add(newsData);
        return results;
    }
}

package com.example.guiberto.simplelearningapp;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class QuizActivity extends AppCompatActivity {
    TextView questiontitle;

    private DatabaseReference mFirebaseDatabaseReference;
    List<DataSnapshot> m_quList;
    public int right, wrong;
    String quizanswer;
    String selectedFromList;
    int m_quIndex = 0;
    Button nextbtn;
    TextView questioncount;
    int quecount;
    int i = 65;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizunitscreen);
        questiontitle = (TextView)findViewById(R.id.questiontitle);
        questioncount = (TextView)findViewById(R.id.questionnum);
        nextbtn = (Button)findViewById(R.id.nextbutton);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        m_quList = new ArrayList<DataSnapshot>();

        DatabaseReference v1 = mFirebaseDatabaseReference.child("v1/components/com100/custom_attributes/questions");


        v1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> questions = dataSnapshot.getChildren();

                for (DataSnapshot q : questions) {
                    m_quList.add(q);
                }

                m_quIndex = 0;
                getQuestion(m_quIndex);


                quecount = (int)dataSnapshot.getChildrenCount();

                if(quecount > 0) {
                    questioncount.setText(String.format("Question %d of %d", m_quIndex+1, quecount));
                }
                else
                    Toast.makeText(QuizActivity.this, "No Questions", Toast.LENGTH_SHORT).show();
 //               for (DataSnapshot course : courses) {

//                    QuestionsTitle model = dataSnapshot.getValue(QuestionsTitle.class);
//                    data.add(model);
//                    questiontitle.setText(model.title);
  //              }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


//        DatabaseReference v3 = mFirebaseDatabaseReference.child("v1/questions/qu100/options");
//        v3.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot1) {
//                Iterable<DataSnapshot> courses1 = dataSnapshot1.getChildren();
//                final ArrayList<QuestionListView_Cell> results = new ArrayList<QuestionListView_Cell>();
//                //final int quecount = (int)dataSnapshot1.getChildrenCount();
//
//                for (DataSnapshot course1 : courses1) {
//                    String k = course1.getKey();
//
//                    DatabaseReference v4 = mFirebaseDatabaseReference.child("v1/options/" + k);
//                    v4.addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            ListView view = (ListView)findViewById(R.id.questionslist);
//
//
//                            QuestionListView_Cell chat = dataSnapshot.getValue(QuestionListView_Cell.class);
//                            QuestionListView_Cell subData = new QuestionListView_Cell();
//                            subData.title = chat.getTitle();
//
//                            //for(int i = 65; i < 65 + quecount; i++) {
//                            subData.quemark = new Character((char) i).toString();
//                            i++;
//                            //}
//
//                            results.add(subData);
//                            Log.d("log", chat.getTitle());
//
//
//
//                            setSubListView(view, results);
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//
////                    QuestionsTitle model = dataSnapshot1.getValue(QuestionsTitle.class);
////                    data.add(model);
////                    questiontitle.setText(model.title);
//                }
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });


        nextbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ShowNextQuestion();

            }
        });
    }


    protected void getQuestion(int qIdx) {
        String quKey = m_quList.get(qIdx).getKey();

        questioncount.setText(String.format("Question %d of %d", qIdx + 1, quecount));

        final DatabaseReference v1 = mFirebaseDatabaseReference.child("v1/questions/" + quKey);
        v1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                QuestionsTitle model = dataSnapshot.getValue(QuestionsTitle.class);
                //data.add(model);
                questiontitle.setText(model.getTitle());
                //quizanswer = model.answer;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        final DatabaseReference v2 = mFirebaseDatabaseReference.child("v1/questions/" + quKey + "/answer");
        v2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //data.add(model);
                String str_answer = (String)dataSnapshot.getValue();
                DatabaseReference v2_1 = mFirebaseDatabaseReference.child("v1/options/" + str_answer);
                v2_1.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        QuestionListView_Cell chat = dataSnapshot.getValue(QuestionListView_Cell.class);
                        quizanswer = chat.title;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference v3 = mFirebaseDatabaseReference.child("v1/questions/" + quKey + "/options");
        v3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {
                Iterable<DataSnapshot> courses1 = dataSnapshot1.getChildren();
                final ArrayList<QuestionListView_Cell> results = new ArrayList<QuestionListView_Cell>();

                for (DataSnapshot course1 : courses1) {
                    String k = course1.getKey();
                    DatabaseReference v4 = mFirebaseDatabaseReference.child("v1/options/" + k);
                    v4.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ListView view = (ListView)findViewById(R.id.questionslist);


                            QuestionListView_Cell chat = dataSnapshot.getValue(QuestionListView_Cell.class);
                            QuestionListView_Cell subData = new QuestionListView_Cell();
                            subData.title = chat.getTitle();
                            subData.quemark = new Character((char) i).toString();
                            i++;
                            results.add(subData);

                            Log.d("log", chat.getTitle());

                            setSubListView(view, results);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

//                    QuestionsTitle model = dataSnapshot1.getValue(QuestionsTitle.class);
//                    data.add(model);
//                    questiontitle.setText(model.title);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        nextbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ShowNextQuestion();

            }
        });
    }

    public void ShowNextQuestion()
    {
        i = 65;
        if(quizanswer == selectedFromList)
        {
            right++;

        }
        else
            wrong++;
        m_quIndex = m_quIndex + 1;

        if (m_quList.size() > m_quIndex) {
            getQuestion(m_quIndex);
        }
//        else if(m_quList.size() - 1  == m_quIndex)
//            nextbtn.setVisibility(View.GONE);
        else {
            Intent intent = new Intent(QuizActivity.this, ResultActivity.class);
            intent.putExtra("result1",right);
            intent.putExtra("result2",wrong);
            intent.putExtra("quecount",quecount);
            //intent.putExtra()
            startActivity(intent);
            //m_quIndex = m_quList.size() - 1;

        }
    }
    public void setSubListView(View view, ArrayList<QuestionListView_Cell> results){
//        final ListView user_sublists = (ListView)(view.findViewById(R.id.subjectlist));
        final ListView quelists = (ListView) view;
        quelists.setAdapter(new QuestionAdapter(getBaseContext(), results));
        quelists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Object o = quelists.getItemAtPosition(position);
                QuestionListView_Cell newsData = (QuestionListView_Cell) o;

                selectedFromList = newsData.title;
                //Intent intent = new Intent(HomeScreenActivity.this, UnitActivity.class);
                //intent.putExtra("Index",position);
                //startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in.
        // TODO: Add code to check if user is signed in.
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

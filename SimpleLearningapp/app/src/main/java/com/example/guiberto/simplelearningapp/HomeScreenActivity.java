package com.example.guiberto.simplelearningapp;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import it.neokree.materialtabs.MaterialTabHost;

public class HomeScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DatabaseReference mFirebaseDatabaseReference;
    private String courseKey;
    List<String> m_courseList;
    //MaterialTabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_main);

        m_courseList = new ArrayList<String>();

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference v1 = mFirebaseDatabaseReference.child("v1/courses");

        v1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ListView view = (ListView)findViewById(R.id.subjectlist);

                ArrayList<SubListView_Cell> results = new ArrayList<SubListView_Cell>();

                Iterable<DataSnapshot> courses = dataSnapshot.getChildren();
                for (DataSnapshot course : courses) {
                    courseKey = course.getKey();
                    m_courseList.add(courseKey);
                    SubListView_Cell chat = course.getValue(SubListView_Cell.class);

                    SubListView_Cell subData = new SubListView_Cell();
                    subData.name = chat.getName();
                    results.add(subData);

                    Log.d("log", chat.getName());
                }

                setSubListView(view, results);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("Test", "Error loading data:" + error.toString(), error.toException());
            }
        });
        Log.d("log", "Test");

//        getSupportActionBar().hide();

        //Firebase mFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));
        //DatabaseReference ref = FirebaseDatabase.getInstance().getReference("https://mobile-lms-cd560.firebaseio.com/GET/v1/courses");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //setTitle("Hello StackOverflow");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        TextView username = (TextView)(navigationView.getHeaderView(0).findViewById(R.id.user_name));
        username.setText(Global.currentUser.strname);

        TextView useremail = (TextView)(navigationView.getHeaderView(0).findViewById(R.id.user_email));
        useremail.setText(Global.currentUser.stremail);
//
//        ImageView userpic = (ImageView)(navigationView.getHeaderView(0).findViewById(R.id.user_img));
//
//        ImageLoader imgloader = ImageLoader.getInstance();
//        imgloader.init(ImageLoaderConfiguration.createDefault(getBaseContext()));
//        imgloader.displayImage(Global.currentUser.picurl,userpic);
        navigationView.setNavigationItemSelectedListener(this);


    }

    public void setSubListView(View view, ArrayList<SubListView_Cell> results){
//        final ListView user_sublists = (ListView)(view.findViewById(R.id.subjectlist));
        final ListView user_sublists = (ListView) view;
        user_sublists.setAdapter(new SubjectAdapter(getBaseContext(), results));
        user_sublists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Object o = user_sublists.getItemAtPosition(position);
                SubListView_Cell newsData = (SubListView_Cell) o;
                //Toast.makeText(HomeScreenActivity.this, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(HomeScreenActivity.this, UnitActivity.class);
                intent.putExtra("Index",position);
                Global.currentUser.coursekey = m_courseList.get(position);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in.
        // TODO: Add code to check if user is signed in.
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, HomeScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);

//            ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.inclusionlayout);
//
//            View child1 = LayoutInflater.from(this).inflate(
//                    R.layout.app_bar_main, null);
//            inclusionViewGroup.addView(child1);


            // Handle the camera action
        } else if (id == R.id.nav_news) {
            Intent intent = new Intent(this, NewsScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);

//            ViewGroup inclusionViewGroup = (ViewGroup)findViewById(R.id.inclusionlayout);
//
//            View child1 = LayoutInflater.from(this).inflate(
//                    R.layout.app_bar_main1, null);
//            inclusionViewGroup.addView(child1);

        } else if (id == R.id.nav_logout) {
            Login.signOut();
            Intent intent = new Intent(HomeScreenActivity.this, Login.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
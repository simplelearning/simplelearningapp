package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 5/19/16.
 */
public class QuestionsTitle {

    String title;
    String answer;
    public QuestionsTitle(){
    }
    public QuestionsTitle(String title, String answer) {
        this.title = title;
        this.answer = answer;
    }

//    public String getName() {
//        return title;
//    }
//
//    public void setName(String title) {
//        this.title = title;
//    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getAnswer(){return answer;}

    public void setAnswer(String answer){this.answer = answer;}
}

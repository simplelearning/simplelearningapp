package com.example.guiberto.simplelearningapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by mymac on 5/12/16.
 */
public class UnitAdapter extends BaseAdapter {
    private ArrayList<UnitListView_Cell> listData;
    private LayoutInflater layoutInflater;

    ImageView imgStatus;
    public UnitAdapter(Context context, ArrayList<UnitListView_Cell> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null){
            view = layoutInflater.inflate(R.layout.activtiy_unitlistview,null);
            holder = new ViewHolder();



            //holder.txt_subDescription = (TextView)view.findViewById(R.id.sub_decription);
            holder.txt_unitTitle = (TextView)view.findViewById(R.id.unit_title);

           imgStatus = (ImageView) view.findViewById(R.id.imgsuccess);

            if(listData.get(i).completeStatus)
//                if(Global.currentUser.statusflag == 1)
                    imgStatus.setImageResource(R.drawable.complete_img);

            view.setTag(holder);
        }else {
            holder = (ViewHolder)view.getTag();
        }
        //holder.txt_subDescription.setText(listData.get(i).str_subdescription);
        holder.txt_unitTitle.setText(listData.get(i).title);


        return view;
    }
    static class ViewHolder {
        TextView txt_unitTitle;

    }
}

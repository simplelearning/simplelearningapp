package com.example.guiberto.simplelearningapp;

/**
 * Created by mymac on 4/29/16.
 */
public class SubListView_Cell {
    String name;
    String icon_url;
    public SubListView_Cell(){
    }
    public SubListView_Cell(String name, String url) {
        //this.str_subdescription = description;
        this.name = name;
        this.icon_url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl(){
        return icon_url;
    }
    public void setIconUrl(String url)
    {
        this.icon_url = url;
    }
}

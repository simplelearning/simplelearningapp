package com.example.guiberto.simplelearningapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mymac on 5/19/16.
 */
public class QuestionAdapter extends BaseAdapter {
    private ArrayList<QuestionListView_Cell> listData;
    private LayoutInflater layoutInflater;

    public QuestionAdapter(Context context, ArrayList<QuestionListView_Cell> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = layoutInflater.inflate(R.layout.activity_questionlist,null);
            holder = new ViewHolder();
            //holder.txt_subDescription = (TextView)view.findViewById(R.id.sub_decription);
            holder.txt_questionTitle = (TextView)view.findViewById(R.id.question_answer);
            holder.txt_quemark = (TextView)view.findViewById(R.id.question_title);
            view.setTag(holder);
        }else {
            holder = (ViewHolder)view.getTag();
        }
        //holder.txt_subDescription.setText(listData.get(i).str_subdescription);
        holder.txt_questionTitle.setText(listData.get(i).title);
        holder.txt_quemark.setText(listData.get(i).quemark);
        return view;
    }
    static class ViewHolder {
        TextView txt_questionTitle;
        TextView txt_quemark;
        //TextView txt_subDescription;
    }
}
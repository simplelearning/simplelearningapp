package com.example.guiberto.simplelearningapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ComponentsActivity extends AppCompatActivity {
    private DatabaseReference mFirebaseDatabaseReference;
    //List<DataSnapshot> m_typeList;
    ImageButton mediaButton, quizButton;
    String componenttype;
    int m_typeIndex = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_componentscreeen);
        setTitle("Matrix Vector");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        mediaButton = (ImageButton)findViewById(R.id.mediabtn);
        quizButton = (ImageButton)findViewById(R.id.quizbtn);

        Intent intent = getIntent();
        //String str_quizkey = intent.getExtras().getString("QuizKey");

        //m_typeList = new ArrayList<DataSnapshot>();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference component = mFirebaseDatabaseReference.child("v1/units/" + Global.currentUser.quizKey + "/components");
        component.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {
                Iterable<DataSnapshot> courses1 = dataSnapshot1.getChildren();
                //final ArrayList<QuestionListView_Cell> results = new ArrayList<QuestionListView_Cell>();

                for (DataSnapshot course1 : courses1) {
                    String componentkey = course1.getKey();
                    DatabaseReference v4 = mFirebaseDatabaseReference.child("v1/components/" + componentkey);
                    v4.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            ComponentListView_Cell comp = dataSnapshot.getValue(ComponentListView_Cell.class);
                            componenttype = comp.getType();
                            getComponent();
                            Log.d("log", comp.getType());

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference v1 = mFirebaseDatabaseReference.child("v1/videos/vid200");
        v1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                VideoView_Cell chat = dataSnapshot.getValue(VideoView_Cell.class);
                Global.currentUser.videoid = chat.getVideo_id();
                Log.d("log", chat.getVideo_id());
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("Test", "Error loading data:" + error.toString(), error.toException());
            }
        });

        mediaButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ComponentsActivity.this, VideoScreenActivity.class);
                startActivity(intent);
            }
        });


        quizButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ComponentsActivity.this, QuizActivity.class);
                startActivity(intent);
            }
        });


    }

    protected void getComponent()
    {
        if(componenttype.equals("quiz"))
            quizButton.setVisibility(View.VISIBLE);
        else if(componenttype.equals("media"))
            mediaButton.setVisibility(View.VISIBLE);
    }

    private void onBack()
    {
        Intent nextActivity = new Intent(ComponentsActivity.this, UnitActivity.class);
        startActivity(nextActivity);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
